import { Injectable } from '@angular/core';
import { HttpInterceptor, HttpEvent, HttpHandler, HttpRequest, HttpErrorResponse } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { NotificacionService } from 'src/app/shared/service/notificacion.service';

@Injectable()
export class HttpErrorInterceptor implements HttpInterceptor {
  constructor(private notificacionService: NotificacionService) {} 
  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    return next.handle(request).pipe(
      catchError((error: HttpErrorResponse) => {
        if (error.status === 400) {
          this.notificacionService.mostrarToast(error.error,true);
        }

        if (error.status === 404) {
          this.notificacionService.mostrarToast(error.error,true);
        }
        return throwError(error);
      })
    );
  }
}