import { Injectable } from '@angular/core';
import {
  HttpInterceptor,
  HttpRequest,
  HttpHandler,
} from '@angular/common/http';

@Injectable()
export class AuthInterceptor implements HttpInterceptor {
  intercept(request: HttpRequest<any>, next: HttpHandler) {

    const modifiedRequest = request.clone({
      setHeaders: {
        'authorId': '2',
        'Content-Type':'application/json'
      }
    });

    return next.handle(modifiedRequest);
  }
}
