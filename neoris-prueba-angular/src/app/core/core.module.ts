import { ErrorHandler, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { AuthInterceptor } from './Interceptor/http-interceptor';
import { HttpErrorInterceptor } from './Interceptor/http-error-interceptor';



@NgModule({
  declarations: [],
  imports: [
    CommonModule,
  ],
  providers:[
    {
      provide: HTTP_INTERCEPTORS,
      useClass: AuthInterceptor,
      multi: true
    },
    {provide: HTTP_INTERCEPTORS, useClass: HttpErrorInterceptor,multi:true}
  ]
})
export class CoreModule { }
