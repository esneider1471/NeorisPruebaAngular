import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  { path: '', redirectTo: 'productos-financieros/listar-productos-financieros', pathMatch: 'full' },
  {
    path:'productos-financieros',
    loadChildren:()=> import('./feature/producto-financiero/producto-financiero.module').then(m=>m.ProductoFinancieroModule)
  },
  { path: '**', redirectTo: 'productos-financieros/listar-productos-financieros' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
