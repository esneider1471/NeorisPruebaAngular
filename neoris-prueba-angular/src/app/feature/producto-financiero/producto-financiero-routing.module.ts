import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ListarProductoFinancieroComponent } from './componentes/listar-producto-financiero/listar-producto-financiero.component';
import { CrearProductoFinancieroComponent } from './componentes/crear-producto-financiero/crear-producto-financiero.component';

const routes: Routes = [
  {
    path:'',
    children:[
      {path:'listar-productos-financieros', component: ListarProductoFinancieroComponent},
      {path:'crear-productos-financieros', component: CrearProductoFinancieroComponent},
      {path:'crear-productos-financieros/:id', component: CrearProductoFinancieroComponent},
    ]
  }

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ProductoFinancieroRoutingModule { }
