
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { ProductoFinanciero } from 'src/app/shared/model/producto-financiero.model';
import { NotificacionService } from 'src/app/shared/service/notificacion.service';
import { ProductoFinancieroService } from 'src/app/shared/service/producto-financiero.service';

@Component({
  selector: 'app-crear-producto-financiero',
  templateUrl: './crear-producto-financiero.component.html',
  styleUrls: ['./crear-producto-financiero.component.scss'],
})
export class CrearProductoFinancieroComponent implements OnInit {
  formulario: FormGroup = new FormGroup({});
  fechaActual: string = '';
  idpermitido:boolean=false;
  editar:boolean=false;
  productoFinanciero: ProductoFinanciero= new ProductoFinanciero();

  constructor(
    private fb: FormBuilder,
    private productoFinancieroService: ProductoFinancieroService,
    private router: Router,
    private notificacionService: NotificacionService,
    private route: ActivatedRoute
  ) {
    this.initFormProductosFinancieros();
  }

  get idTouched() {
    return this.formulario.controls['id'].touched;
  }

  get idRequired() {
    return this.formulario.controls['id'].errors?.['required'] ? true : false;
  }

  get idInvalido() {
    return this.formulario.controls['id'].status == 'INVALID' &&
      !this.idRequired
      ? true
      : false;
  }

  get nombreTouched() {
    return this.formulario.controls['name'].touched;
  }

  get nombreRequired() {
    return this.formulario.controls['name'].errors?.['required']
      ? true
      : false;
  }

  get nombreValido() {
    return this.formulario.controls['name'].status == 'INVALID' &&
      !this.nombreRequired
      ? true
      : false;
  }

  get descripcionTouched() {
    return this.formulario.controls['description'].touched;
  }

  get descripcionRequired() {
    return this.formulario.controls['description'].errors?.['required']
      ? true
      : false;
  }

  get descripcionValido() {
    return this.formulario.controls['description'].status == 'INVALID' &&
      !this.descripcionRequired
      ? true
      : false;
  }

  get logoTouched() {
    return this.formulario.controls['logo'].touched;
  }

  get logoRequired() {
    return this.formulario.controls['logo'].errors?.['required'] ? true : false;
  }

  get fechaTouched() {
    return this.formulario.controls['date_release'].touched;
  }

  get fechaRequired() {
    return this.formulario.controls['date_release'].errors?.['required']
      ? true
      : false;
  }

  get formularioValido() {
    return this.formulario.status == 'INVALID' || this.idpermitido?true:false;
  }

  ngOnInit() {
    this.retringirFecha();
    this.route.paramMap.subscribe((param)=>{
      if(param.get('id')  && localStorage.getItem('producto')){
        this.editar=true;
        let producto=  localStorage.getItem('producto');
        this.productoFinanciero = producto ?JSON.parse(producto) : null
        this.formulario.patchValue(this.productoFinanciero);
        this.formulario.controls['date_release'].setValue(this.formulario.controls['date_release'].value.match(/\d{4}-\d{2}-\d{2}/)[0]);
        this.formulario.controls['date_revision'].setValue(this.formulario.controls['date_revision'].value.match(/\d{4}-\d{2}-\d{2}/)[0]);
       this.formulario.get('id')?.disable();
      }
    })
  }

  private retringirFecha(){
    const today = new Date();
    this.fechaActual = today.toISOString().split('T')[0];
  }

  private initFormProductosFinancieros() {
    this.formulario = this.fb.group({
      id: [
        '',
        [
          Validators.required,
          Validators.minLength(3),
          Validators.maxLength(10),
        ],
      ],
      name: [
        '',
        [
          Validators.required,
          Validators.minLength(5),
          Validators.maxLength(100),
        ],
      ],
      description: [
        '',
        [
          Validators.required,
          Validators.minLength(10),
          Validators.maxLength(200),
        ],
      ],
      logo: ['', Validators.required],
      date_release: ['', [Validators.required]],
      date_revision: [''],
    });
  }

  validarId(){
    if(!this.idInvalido){
      const id = this.formulario.controls['id'].value;
      this.productoFinancieroService.validarIdProductoFinanciero(id).subscribe(respuesta=>{
        this.idpermitido =respuesta as boolean;
      })
    }
    
  }

  submitForm() {
    if(!this.editar){
      this.productoFinancieroService
      .crearProductoFinanciero(this.formulario.value)
      .subscribe(() => {
        this.notificacionService.mostrarToast('Producto financiero creado con éxito',false);
        this.router.navigateByUrl('/')
      });
    }else{
      this.formulario.value.id=this.productoFinanciero.id;
      this.productoFinancieroService
      .actualizarProductoFinanciero(this.formulario.value)
      .subscribe(() => {
        this.notificacionService.mostrarToast('Producto financiero actualizado con éxito',false);
        this.router.navigateByUrl('/')
      });
    }
  
    
  }

  limpiarFormulario() {
    this.formulario.reset();
  }

  setFechaRevision() {
    let fecha = this.formulario.controls['date_release'].value;
    let fechaRevision = new Date(
      `${Number(fecha.substring(0, 4)) + 1}-${fecha.substring(
        5,
        7
      )}-${fecha.substring(8, 10)}`
    );

    const ano = fechaRevision.getFullYear();
    const mes = (fechaRevision.getMonth() + 1).toString().padStart(2, '0');
    const dia = fechaRevision.getDate().toString().padStart(2, '0');
    this.formulario.get('date_revision')?.setValue(`${ano}-${mes}-${dia}`);
    this.formulario.value.date_revision= this.formulario.controls['date_revision'].value;
  }

}
