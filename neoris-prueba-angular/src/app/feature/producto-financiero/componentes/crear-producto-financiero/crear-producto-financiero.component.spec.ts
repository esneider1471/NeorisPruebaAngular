import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';

import { CrearProductoFinancieroComponent } from './crear-producto-financiero.component';
import { FormBuilder, FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ProductoFinancieroService } from 'src/app/shared/service/producto-financiero.service';
import { HttpClient, HttpHandler } from '@angular/common/http';
import { RouterTestingModule } from '@angular/router/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';
import { NotificacionService } from 'src/app/shared/service/notificacion.service';

describe('CrearProductoFinancieroComponent', () => {
  let component: CrearProductoFinancieroComponent;
  let fixture: ComponentFixture<CrearProductoFinancieroComponent>;
  let productoFinancieroService : ProductoFinancieroService;
  let notificacionService: NotificacionService;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CrearProductoFinancieroComponent ],
      imports:[FormsModule,ReactiveFormsModule,RouterTestingModule],
      providers:[ProductoFinancieroService,HttpClient,HttpHandler,NotificacionService,
        {
          provide: ActivatedRoute,
          useValue: {
            paramMap: {
              subscribe: (fn: (value: any) => void) => {
                fn({
                  get: (key: string) => 'some_id' // Simular el valor de 'id'
                });
              }
            }
          }
        },
        FormBuilder]

    })
    .compileComponents();

    fixture = TestBed.createComponent(CrearProductoFinancieroComponent);
    component = fixture.componentInstance;
    productoFinancieroService= TestBed.inject(ProductoFinancieroService);
    notificacionService= TestBed.inject(NotificacionService);
    spyOn(productoFinancieroService,'validarIdProductoFinanciero').and.returnValue(of(true));
    spyOn(productoFinancieroService,'crearProductoFinanciero').and.returnValue(of(true));
    spyOn(productoFinancieroService,'actualizarProductoFinanciero').and.returnValue(of(true));
    spyOn(notificacionService,'mostrarToast').and.resolveTo();
    component['initFormProductosFinancieros']();
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('es idRequired false', () => {
    expect(component.idRequired).toBeFalse();
  });

  it('es idRequired false', () => {
    component.formulario.controls['id'].setValue('prueba1');
    expect(component.idRequired).toBeFalse();
  });

  it('es idInvalido false', () => {
    component.formulario.controls['id'].setValue('pr');
    expect(component.idInvalido).toBeFalse();
  });

  it('es idInvalido false', () => {
    component.formulario.controls['id'].setValue('prrueba1');
    expect(component.idInvalido).toBeFalse();
  });


  it('es nombreRequired false', () => {
    expect(component.nombreRequired).toBeFalse();
  });

  it('es nombreRequired false', () => {
    component.formulario.controls['name'].setValue('prueba1');
    expect(component.nombreRequired).toBeFalse();
  });

  it('es nombreValido true', () => {
    component.formulario.controls['name'].setValue('prue');
    expect(component.nombreValido).toBeTrue();
  });

  it('es nombreValido false', () => {
    component.formulario.controls['name'].setValue('prueba 1');
    expect(component.nombreValido).toBeFalse();
  });

  it('es descripcionRequired false', () => {
    expect(component.descripcionRequired).toBeFalse();
  });

 it('es descripcionRequired false', () => {
    component.formulario.controls['description'].setValue('prueba 1 producto');
    expect(component.descripcionRequired).toBeFalse();
  });

  it('es descripcionValido true', () => {
    component.formulario.controls['description'].setValue('prueba1');
    expect(component.descripcionValido).toBeTrue();
  });

  it('es descripcionValido false', () => {
    component.formulario.controls['description'].setValue('prueba1 producto');
    expect(component.descripcionValido).toBeFalse();
  });

  it('es logoRequired false', () => {
    expect(component.logoRequired).toBeFalse();
  });

  it('es logoRequired false', () => {
    component.formulario.controls['logo'].setValue('url logo');
    expect(component.logoRequired).toBeFalse();
  });

  it('es logoRequired false', () => {
    expect(component.fechaRequired).toBeFalse();
  });

  it('es fechaRequired false', () => {
    component.formulario.controls['date_release'].setValue('26/08/2023');
    expect(component.fechaRequired).toBeFalse();
  });

  it('es formularioValido true', () => {
    component.idpermitido=true;
    expect(component.formularioValido).toBeTrue();
  });

  it('deberia editar cuandon hay producto existente en localStorage', () => {
    spyOn(localStorage, 'getItem').and.returnValue('{"key": "value"}');
    component.formulario.controls['date_release'].setValue("2025-08-03T00:00:00.000+00:00");
    component.formulario.controls['date_revision'].setValue("2025-08-03T00:00:00.000+00:00");
    component.ngOnInit();
    expect(component.editar).toBe(true);
  });

  it('validar  validarId funcion', () => {
    component.formulario.controls['id'].setValue('prrueba1');
    component.validarId();
    expect(productoFinancieroService.validarIdProductoFinanciero).toHaveBeenCalled();
  });

  it('validar  submitForm crear', () => {
    component.editar = false;
    component.submitForm();
    expect(productoFinancieroService.crearProductoFinanciero).toHaveBeenCalled();
    expect(notificacionService.mostrarToast).toHaveBeenCalled();
  });

  it('validar  submitForm actualizar', () => {
    component.editar = true;
    component.submitForm();
    expect(productoFinancieroService.actualizarProductoFinanciero).toHaveBeenCalled();
    expect(notificacionService.mostrarToast).toHaveBeenCalled();
  });

  it('validar  limpiarFormulario funcion', () => {
    component.formulario.controls['date_release'].setValue('26/08/2023');
    component.limpiarFormulario();
    expect( component.formulario.controls['date_release'].value).toBe(null);
  });

  it('validar  setFechaRevision funcion', fakeAsync(() => {
    component.formulario.controls['date_release'].setValue('26/08/2023');
    component.setFechaRevision();
    tick(10);
    expect( component.formulario.controls['date_revision'].value).toEqual('2001-02-23');
  }));


});
