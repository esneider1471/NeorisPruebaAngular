import { ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ProductoFinanciero } from 'src/app/shared/model/producto-financiero.model';
import { NotificacionService } from 'src/app/shared/service/notificacion.service';
import { ProductoFinancieroService } from 'src/app/shared/service/producto-financiero.service';

@Component({
  selector: 'app-listar-producto-financiero',
  templateUrl: './listar-producto-financiero.component.html',
  styleUrls: ['./listar-producto-financiero.component.scss']
})
export class ListarProductoFinancieroComponent implements OnInit {

   listaPorductosFinancieros:ProductoFinanciero[]=[];
   items: any[] = [];
   itemsPerPage: number = 5; 
   currentPage: number = 1;
   isMenuOpen: string | null = null;
   searchTerm:string='';
   cargando: boolean=false;


  constructor(private productoFinancieroService: ProductoFinancieroService,
              private router:Router) { }

  ngOnInit() {
    this.listarProductosFinancieros();
  }

  listarProductosFinancieros(){
    this.productoFinancieroService.listarProductosFinancieros().subscribe(respuesta=>{
      this.listaPorductosFinancieros=respuesta;
    })
  }

  paginarItems(): any[] {
    const startIndex = (this.currentPage - 1) * this.itemsPerPage;
    const endIndex = startIndex + this.itemsPerPage;
    return this.listaPorductosFinancieros.slice(startIndex, endIndex);
  }

  toggleMenu(itemId: string) {
    if (this.isMenuOpen === itemId) {
      this.isMenuOpen = null;
    } else {
      this.isMenuOpen = itemId;
    }
  }

  editarProducto(producto: ProductoFinanciero) {
    localStorage.setItem('producto',JSON.stringify(producto));
    this.router.navigate(['/productos-financieros/crear-productos-financieros',producto.id]);
  }

  eliminarProducto(id:string) {
    this.toggleMenu(id);
    this.productoFinancieroService.eliminarProductoFinanciero(id).subscribe();
    setTimeout(() => {
      this.listarProductosFinancieros();
    }, 1000);
    
  }

}
