import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';

import { ListarProductoFinancieroComponent } from './listar-producto-financiero.component';
import { ProductoFinancieroService } from 'src/app/shared/service/producto-financiero.service';
import { HttpClient, HttpHandler } from '@angular/common/http';
import { BusquedaPipe } from 'src/app/shared/pipe/busqueda.pipe';
import { SharedModule } from 'src/app/shared/shared.module';
import { NotificacionService } from 'src/app/shared/service/notificacion.service';
import { of } from 'rxjs';
import { ProductoFinanciero } from 'src/app/shared/model/producto-financiero.model';
import { RouterTestingModule } from '@angular/router/testing';
import { Router } from '@angular/router';

describe('ListarProductoFinancieroComponent', () => {
  let component: ListarProductoFinancieroComponent;
  let fixture: ComponentFixture<ListarProductoFinancieroComponent>;
  let productoFinancieroService : ProductoFinancieroService;
  let notificacionService: NotificacionService;
  let ProductoFinanciero: ProductoFinanciero[]= [{date_release:'2020-08-10',date_revision:'2021-08-10',description:'prueba',id:'idtest',logo:'url',name:'prueba'}];

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ListarProductoFinancieroComponent ],
      imports:[SharedModule,RouterTestingModule],
      providers:[ProductoFinancieroService,HttpClient,HttpHandler,NotificacionService]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ListarProductoFinancieroComponent);
    component = fixture.componentInstance;
    productoFinancieroService= TestBed.inject(ProductoFinancieroService);
    notificacionService= TestBed.inject(NotificacionService);
    spyOn(productoFinancieroService,'listarProductosFinancieros').and.returnValue(of(ProductoFinanciero));
    spyOn(productoFinancieroService,'eliminarProductoFinanciero').and.returnValue(of(true));
    spyOn(notificacionService,'mostrarToast').and.resolveTo();
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('validar  listarProductosFinancieros funcion', () => {
    component.listarProductosFinancieros();
    expect(productoFinancieroService.listarProductosFinancieros).toHaveBeenCalled();
  });

  it('validar  toggleMenu funcion', () => {
    component.isMenuOpen='1';
    component.toggleMenu('1');
    expect(component.isMenuOpen).toBeNull();

    component.isMenuOpen='1';
    component.toggleMenu('2');
    expect(component.isMenuOpen).toEqual('2');
  });

  it('should update product and navigate to the correct URL', () => {
    
    const routerSpy = spyOn(TestBed.inject(Router), 'navigate');

    component.editarProducto(ProductoFinanciero[0]);

    expect(localStorage.getItem('producto')).toEqual(JSON.stringify(ProductoFinanciero[0]));
    expect(routerSpy).toHaveBeenCalledWith(['/productos-financieros/crear-productos-financieros', ProductoFinanciero[0].id]);
  });

  it('validar  eliminarProducto funcion', fakeAsync(() => {
    const listarSpy = spyOn(component, 'listarProductosFinancieros');
    component.eliminarProducto('1');
    expect(productoFinancieroService.eliminarProductoFinanciero).toHaveBeenCalled();
    tick(1000);
    expect(listarSpy).toHaveBeenCalled();
  }));
});
