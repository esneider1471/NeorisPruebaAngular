import { NgModule } from '@angular/core';
import { CommonModule, DatePipe } from '@angular/common';

import { ProductoFinancieroRoutingModule } from './producto-financiero-routing.module';
import { ListarProductoFinancieroComponent } from './componentes/listar-producto-financiero/listar-producto-financiero.component';
import { SharedModule } from 'src/app/shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CrearProductoFinancieroComponent } from './componentes/crear-producto-financiero/crear-producto-financiero.component';
import { CoreModule } from 'src/app/core/core.module';


@NgModule({
  declarations: [
    ListarProductoFinancieroComponent,
    CrearProductoFinancieroComponent
  ],
  imports: [
    CommonModule,
    ProductoFinancieroRoutingModule,
    SharedModule,
    FormsModule,
    ReactiveFormsModule,
    CoreModule
  ],
  providers:[DatePipe]
})
export class ProductoFinancieroModule { }
