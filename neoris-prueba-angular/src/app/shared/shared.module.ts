import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { ProductoFinancieroService } from './service/producto-financiero.service';
import { ToastNotificacionComponent } from './componentes/toast-notificacion/toast-notificacion.component';
import { BusquedaPipe } from './pipe/busqueda.pipe';



@NgModule({
  declarations: [
    ToastNotificacionComponent,
    BusquedaPipe
  ],
  imports: [
    CommonModule
  ],
  exports:[HttpClientModule,ToastNotificacionComponent,BusquedaPipe]
  ,
  providers:[ProductoFinancieroService,BusquedaPipe]
})
export class SharedModule { }
