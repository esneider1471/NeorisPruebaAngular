import { Component, Input, OnInit } from '@angular/core';
import { NotificacionService } from '../../service/notificacion.service';

@Component({
  selector: 'app-toast-notificacion',
  templateUrl: './toast-notificacion.component.html',
  styleUrls: ['./toast-notificacion.component.scss']
})
export class ToastNotificacionComponent implements OnInit {

  mensaje: string='';
  visible: boolean = false;
  error:boolean=false;
  

  constructor(private notificationService: NotificacionService) {}

  ngOnInit() {
    this.notificationService.toast$.subscribe(mensaje => {
      this.mensaje=mensaje.mensaje,
      this.error=mensaje.error;
      this.mostrar();
    });
  }

  mostrar() {
    this.visible = true;
    setTimeout(() => {
      this.ocultar();
    }, 3000);
  }

  ocultar() {
    this.visible = false;
  }
}
