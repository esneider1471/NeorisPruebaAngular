import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';

import { ToastNotificacionComponent } from './toast-notificacion.component';
import { NotificacionService } from '../../service/notificacion.service';
import { Subject } from 'rxjs';

describe('ToastNotificacionComponent', () => {
  let component: ToastNotificacionComponent;
  let fixture: ComponentFixture<ToastNotificacionComponent>;
  let notificacionService: NotificacionService;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ToastNotificacionComponent ],
      providers:[NotificacionService]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ToastNotificacionComponent);
    component = fixture.componentInstance;
    notificacionService= TestBed.inject(NotificacionService);
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('deberia llamar mostrar() en toast$ subscription', () => {
    const mockMensaje = { mensaje: 'Mensaje de prueba', error: false };
    const mostrarSpy = spyOn(component, 'mostrar');

    notificacionService.mostrarToast('Mensaje de prueba',false);

    expect(component.mensaje).toEqual(mockMensaje.mensaje);
    expect(component.error).toEqual(mockMensaje.error);

    expect(mostrarSpy).toHaveBeenCalled();
  });

  it('deberia poner visible en  true y  llamar ocultar()', fakeAsync(() => {
    const ocultarSpy = spyOn(component, 'ocultar');

    component.mostrar();

    expect(component.visible).toBe(true);

    tick(3000);

    expect(ocultarSpy).toHaveBeenCalled();

    expect(component.visible).toBe(true);
  }));

  it('deberia llamar la funcion ocultar() y poner visible en false', () => {
    
    component.ocultar();
    expect(component.visible).toBe(false);
  });
});
