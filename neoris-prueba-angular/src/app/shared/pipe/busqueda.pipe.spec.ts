import { BusquedaPipe } from './busqueda.pipe';

describe('BusquedaPipe', () => {

  let pipe: BusquedaPipe;

  beforeEach(() => {
    pipe = new BusquedaPipe();
  });

  it('create an instance', () => {
    const pipe = new BusquedaPipe();
    expect(pipe).toBeTruthy();
  });

  
it('rtorna vacio si no hay items', () => {
  const result = pipe.transform([], 'searchTerm');
  expect(result).toEqual([]);
});

it('deberia retornar los mismos elementos', () => {
  const items = [{ name: 'item1' }, { name: 'item2' }];
  const result = pipe.transform(items,'');
  expect(result).toEqual(items);
});

it('deberia retornar resultados basado en la busqueda', () => {
  const items = [
    { name: 'apple' },
    { name: 'banana' },
    { name: 'grape' },
  ];

  const result = pipe.transform(items, 'ba');

  expect(result).toEqual([{ name: 'banana' }]);
});

});
