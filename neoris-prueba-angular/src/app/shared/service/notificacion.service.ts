import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class NotificacionService {

  private toastSubject = new Subject<any>();
  toast$ = this.toastSubject.asObservable();

  mostrarToast(mensaje: string,error:boolean) {
    this.toastSubject.next({mensaje:mensaje,error:error});
  }
}
