import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { ProductoFinanciero } from '../model/producto-financiero.model';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';

@Injectable()
export class ProductoFinancieroService {

  constructor(private http: HttpClient) { }

  listarProductosFinancieros():Observable<ProductoFinanciero[]>{
  
    return this.http.get<ProductoFinanciero[]>(environment.urlBp);
  }

  validarIdProductoFinanciero(id: string){
    const params = new HttpParams()
      .set('id', id)
    return this.http.get(environment.urlBpVerification,{params})
  }

  crearProductoFinanciero(producto:ProductoFinanciero){
    return this.http.post(environment.urlBp,producto);
  }

  actualizarProductoFinanciero(producto:ProductoFinanciero){
    return this.http.put(environment.urlBp,producto);
  }

   eliminarProductoFinanciero(id:string){
    const params = new HttpParams()
      .set('id', id)
    return this.http.delete(environment.urlBp,{params});
  }


}
