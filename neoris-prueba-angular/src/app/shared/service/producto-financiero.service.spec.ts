import { TestBed } from '@angular/core/testing';
import { ProductoFinanciero } from 'src/app/shared/model/producto-financiero.model';
import { ProductoFinancieroService } from './producto-financiero.service';
import {HttpTestingController,HttpClientTestingModule} from '@angular/common/http/testing'
import { environment } from 'src/environments/environment';

describe('ProductoFinancieroService', () => {
  let service: ProductoFinancieroService;
  let httpController: HttpTestingController;
  let productoFinanciero: ProductoFinanciero[]= [{date_release:'2020-08-10',date_revision:'2021-08-10',description:'prueba',id:'idtest',logo:'url',name:'prueba'}];

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports:[HttpClientTestingModule],
      providers:[ProductoFinancieroService]
    });
    service = TestBed.inject(ProductoFinancieroService);
    httpController = TestBed.inject(HttpTestingController);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  
  it('deveria listar Productos financieros', () => {
    service.listarProductosFinancieros().subscribe(respuesta=>{
      expect(respuesta).toEqual(productoFinanciero);
    });
    const req= httpController.expectOne({
      method:'GET',
      url: `${environment.urlBp}`
    });

    req.flush(productoFinanciero);
  });

  it('deveria validar Id  Productos financieros', () => {
    service.validarIdProductoFinanciero('test').subscribe(respuesta=>{
      expect(respuesta).toEqual(true);
    });
    const req= httpController.expectOne({
      method:'GET',
      url: `${environment.urlBpVerification}?id=test`
    });

    req.flush(true);
  });

  it('deveria crear Productos financieros', () => {
    service.crearProductoFinanciero(productoFinanciero[0]).subscribe(respuesta=>{
      expect(respuesta).toEqual(productoFinanciero[0]);
    });
    const req= httpController.expectOne({
      method:'POST',
      url: `${environment.urlBp}`
    });

    req.flush(productoFinanciero[0]);
  });

  it('deveria actualizar Productos financieros', () => {
    service.actualizarProductoFinanciero(productoFinanciero[0]).subscribe(respuesta=>{
      expect(respuesta).toEqual(productoFinanciero[0]);
    });
    const req= httpController.expectOne({
      method:'PUT',
      url: `${environment.urlBp}`
    });

    req.flush(productoFinanciero[0]);
  });

  it('deveria eliminar Productos financieros', () => {
    service.eliminarProductoFinanciero('test').subscribe(respuesta=>{
      expect(respuesta).toEqual(true);
    });
    const req= httpController.expectOne({
      method:'DELETE',
      url: `${environment.urlBp}?id=test`
    });

    req.flush(true);
  });
});
